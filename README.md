# Contribution for tee_se_transport_manager #

tee_se_transport_manager mainly includes channel management and SCP protocol. The following describes the modules

### 1. The specific module introduction of tee_se_transport_manager ###
<table>
<th>Name of module</th>
<th>Functions</th>
<tr>
<td> Manage Channels </td><td>Creates the channel logic for interacting with the chip</td>
</tr><tr>
<td> SCP protocol</td><td>Creates the SCP03 protocol</td>
</tr>
</table>

### 2. tee_os_framework code directories ###
```
base/tee/tee_se_transport_manager
├── build
│   ├── lite
│   │   ├── build_client
│   │   └── build_service
│   └── standard
│       ├── build_client
│       └── build_service
├── bundle.json
├── LICENSE
├── README.md
├── README_zh.md
├── secure_channel
├── se_driver_hal
│   ├── nontee
│   │   └── se_drv_hal
│   └── tee
│       └── se_drv_hal
├── se_service
│   ├── include
│   └── src
│       ├── se_ipc
│       └── se_service_main
├── se_transport
│   ├── include
│   └── src
│       ├── se_api
│       └── se_ipc_client
└── test
    └── unittest
```